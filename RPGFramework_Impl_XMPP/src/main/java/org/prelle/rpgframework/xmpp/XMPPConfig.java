/**
 * 
 */
package org.prelle.rpgframework.xmpp;

import java.util.Properties;

/**
 * @author prelle
 *
 */
public class XMPPConfig {

	private String identity;
	private String password;
	private String serverHost;
	private int serverPort = 5222;
	private String resource;
	
	//------------------------------------------------------------------
	public XMPPConfig() {		
	}
	
	//------------------------------------------------------------------
	public XMPPConfig(Properties pro) {
		readFromProperties(pro);
	}
	
	//------------------------------------------------------------------
	public void readFromProperties(Properties pro) {
		identity = pro.getProperty("xmpp.user");
		password = pro.getProperty("xmpp.pass");
		serverHost = pro.getProperty("xmpp.server");
		serverPort = Integer.parseInt(pro.getProperty("xmpp.port", "5222"));
		resource = pro.getProperty("xmpp.resource");
	}
	
	//------------------------------------------------------------------
	public void writeToProperties(Properties pro) {
		if (identity!=null) pro.setProperty("xmpp.user", identity);
		if (password!=null) pro.setProperty("xmpp.pass", password);
		if (serverHost!=null) pro.setProperty("xmpp.server", serverHost);
		if (serverPort!=5222) pro.setProperty("xmpp.port", serverPort+"");
		if (resource!=null) pro.setProperty("xmpp.resource", resource);
	}
	
	//------------------------------------------------------------------
	/**
	 * @return the identity
	 */
	public String getIdentity() {
		return identity;
	}
	
	//------------------------------------------------------------------
	/**
	 * @param identity the identity to set
	 */
	public void setIdentity(String identity) {
		this.identity = identity;
	}
	
	//------------------------------------------------------------------
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	
	//------------------------------------------------------------------
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	
	//------------------------------------------------------------------
	/**
	 * @return the serverHost
	 */
	public String getServerHost() {
		return serverHost;
	}
	
	//------------------------------------------------------------------
	/**
	 * @param serverHost the serverHost to set
	 */
	public void setServerHost(String serverHost) {
		this.serverHost = serverHost;
	}
	//------------------------------------------------------------------
	/**
	 * @return the serverPort
	 */
	public int getServerPort() {
		return serverPort;
	}
	//------------------------------------------------------------------
	/**
	 * @param serverPort the serverPort to set
	 */
	public void setServerPort(int serverPort) {
		this.serverPort = serverPort;
	}

	//------------------------------------------------------------------
	/**
	 * @return the resource
	 */
	public String getResource() {
		return resource;
	}

	//------------------------------------------------------------------
	/**
	 * @param resource the resource to set
	 */
	public void setResource(String resource) {
		this.resource = resource;
	}
	
	
}
