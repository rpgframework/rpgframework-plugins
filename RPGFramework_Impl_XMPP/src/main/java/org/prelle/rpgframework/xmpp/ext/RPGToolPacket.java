/**
 * 
 */
package org.prelle.rpgframework.xmpp.ext;

import org.jivesoftware.smack.packet.PacketExtension;

/**
 * @author prelle
 *
 */
public abstract class RPGToolPacket implements PacketExtension {

	private MessageType type;
	
	//------------------------------------------------------------------
	/**
	 */
	protected RPGToolPacket(MessageType type) {
		this.type = type;
	}
	
	//------------------------------------------------------------------
	public MessageType getType() {
		return type;
	}

	//------------------------------------------------------------------
	/**
	 * @see org.jivesoftware.smack.packet.PacketExtension#getNamespace()
	 */
	@Override
	public String getNamespace() {
		return RPGToolExtensionProvider.NAMESPACE;
	}

}
