/**
 * 
 */
package org.prelle.rpgtool.xmpp;

import org.apache.log4j.Logger;
import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.PacketExtension;
import org.prelle.rpgtool.RPGToolDevice;
import org.prelle.rpgtool.Role;
import org.prelle.rpgtool.SessionScreen;
import org.prelle.rpgtool.xmpp.ext.RPGToolPacket;
import org.prelle.rpgtool.xmpp.ext.ShowTextMessage;

/**
 * @author prelle
 *
 */
public class XMPPSessionScreenServer implements XMPPClientCallback {

	private final static Logger logger = Logger.getLogger("rpgtool.xmpp");
	
	private SessionScreen realScreen;
	private XMPPClient xmppClient;
	
	//------------------------------------------------------------------
	public XMPPSessionScreenServer(XMPPClient client, SessionScreen screen) {
		this.realScreen = screen;
		this.xmppClient = client;
//		xmppClient = new XMPPClient(Role.SESSION_SCREEN, (XMPPClientCallback)this);
//		xmppClient.setConfig(config);
//		xmppClient.start();
	}

	//------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgtool.xmpp.XMPPClientCallback#messageReceived(org.prelle.rpgtool.RPGToolDevice, org.jivesoftware.smack.Chat, org.jivesoftware.smack.packet.Message)
	 */
	@Override
	public void messageReceived(RPGToolDevice device, Chat chat, Message message) {
		// TODO Auto-generated method stub
		logger.info("Message received from "+device.getDeviceType()+" "+chat.getParticipant()+": "+message);
		
		for (PacketExtension ext : message.getExtensions()) {
			logger.debug("  EXT "+ext);
			if (ext instanceof RPGToolPacket) {
				RPGToolPacket packet = (RPGToolPacket)ext;
				switch (packet.getType()) {
				case SEND_TEXT_MESSAGE:
					realScreen.showTextMessage(
							((ShowTextMessage)packet).getText(), 
							((ShowTextMessage)packet).getSeconds()
							);
					break;
				default:
					logger.warn("Don't know what to do with "+packet.getType());
				}
			}
		}
	}
	
}
