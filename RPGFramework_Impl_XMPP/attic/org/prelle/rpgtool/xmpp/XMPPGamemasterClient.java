/**
 * 
 */
package org.prelle.rpgtool.xmpp;

import javax.swing.Icon;

import org.apache.log4j.Logger;
import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.packet.Message;
import org.prelle.rpgtool.Gamemaster;

/**
 * @author prelle
 *
 */
public class XMPPGamemasterClient implements Gamemaster, MessageListener {
	
	private final static Logger logger = Logger.getLogger("rpgtool.xmpp");

	private String username;
	private XMPPConnection connection;
	private Chat chat;

	//------------------------------------------------------------------
	/**
	 */
	public XMPPGamemasterClient(String username, XMPPConnection conn) {
		this.username = username;
		this.connection = conn;
		chat = connection.getChatManager().createChat(username, this);
	}

	//------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgtool.RPGToolDevice#getUserName()
	 */
	@Override
	public String getUserName() {
		return username;
	}

	//------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgtool.RPGToolDevice#getDeviceType()
	 */
	@Override
	public Type getDeviceType() {
		return Type.GAMEMASTER;
	}

	//------------------------------------------------------------------
	/**
	 * @see org.jivesoftware.smack.MessageListener#processMessage(org.jivesoftware.smack.Chat, org.jivesoftware.smack.packet.Message)
	 */
	@Override
	public void processMessage(Chat chat, Message message) {
		logger.debug("Message received from session screen: "+message);
	}

	//------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgtool.RPGToolDevice#getIcon()
	 */
	@Override
	public Icon getIcon() {
		// TODO Auto-generated method stub
		return null;
	}

}
