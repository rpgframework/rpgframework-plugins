/**
 * 
 */
package org.prelle.rpgtool.xmpp.ext;

/**
 * @author prelle
 *
 */
public class ShowTextMessage extends RPGToolPacket {

	private String text;
	private int seconds;
	
	//------------------------------------------------------------------
	/**
	 * @param type
	 */
	public ShowTextMessage(String text, int seconds) {
		super(MessageType.SEND_TEXT_MESSAGE);
		this.text = text;
		this.seconds = seconds;
	}

	//------------------------------------------------------------------
	/**
	 * @see org.jivesoftware.smack.packet.PacketExtension#getElementName()
	 */
	@Override
	public String getElementName() {
		return "showtext";
	}

	//------------------------------------------------------------------
	/**
	 * @see org.jivesoftware.smack.packet.PacketExtension#toXML()
	 */
	@Override
	public String toXML() {
		 return "<rpgtool xmlns='"+RPGToolExtensionProvider.NAMESPACE+"'>" +
		         "<showtext sec=\""+seconds+"\">" + text +"</showtext>" +
		         "</rpgtool>";
	}

	//------------------------------------------------------------------
	/**
	 * @return The text to display
	 */
	public String getText() {
		return text;
	}

	//------------------------------------------------------------------
	/**
	 * @return Number of seconds
	 */
	public int getSeconds() {
		return seconds;
	}

}
