/**
 * 
 */
package org.prelle.rpgtool.xmpp.ext;

import org.apache.log4j.Logger;
import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.packet.Message;
import org.prelle.rpgtool.RPGToolDevice;
import org.prelle.rpgtool.xmpp.XMPPClientCallback;

/**
 * @author prelle
 *
 */
public class DummyCallback implements XMPPClientCallback {
	
	private final static Logger logger = Logger.getLogger("xmpp.callback");

	//------------------------------------------------------------------
	/**
	 */
	public DummyCallback() {
		// TODO Auto-generated constructor stub
	}

	//------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgtool.xmpp.XMPPClientCallback#messageReceived(org.prelle.rpgtool.RPGToolDevice, org.jivesoftware.smack.Chat, org.jivesoftware.smack.packet.Message)
	 */
	@Override
	public void messageReceived(RPGToolDevice device, Chat chat, Message message) {
		// TODO Auto-generated method stub
		logger.info("messageReceived");
	}

}
