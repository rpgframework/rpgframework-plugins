/**
 * 
 */
package org.prelle.rpgtool.xmpp.ext;

import java.util.ArrayList;
import java.util.List;

import javax.activation.MimeType;
import javax.activation.MimeTypeParseException;

import org.apache.log4j.Logger;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;
//import org.prelle.rpgtool.sessionscreen.HandoutDisplay;
import org.prelle.rpgtool.xmpp.XMPPSessionScreenClient;

/**
 * @author prelle
 *
 */
public class HandoutClientImpl { // implements HandoutDisplay {

	private final static Logger logger = Logger.getLogger("rpg.xmpp.sscreen");
	
	private final static String[] MIME_TYPES = new String[]{
		"image/jpeg",
		"image/gif",
		"image/png"
	};

	private XMPPSessionScreenClient client;
	private List<MimeType> mimeTypes;

	//----------------------------------------------------------------
	/**
	 */
	public HandoutClientImpl(XMPPSessionScreenClient client) {
		this.client = client;
		mimeTypes = new ArrayList<MimeType>();
		for (String mime : MIME_TYPES) {
			try {
				mimeTypes.add(new MimeType(mime));
			} catch (MimeTypeParseException e) {
				logger.error(e.toString(),e);
			}
		}
	}

//	//----------------------------------------------------------------
//	/* (non-Javadoc)
//	 * @see org.prelle.rpgtool.sessionscreen.SessionScreenExtension#getIdentifier()
//	 */
//	@Override
//	public String getIdentifier() {
//		return HandoutDisplay.NAME;
//	}
//
//	//----------------------------------------------------------------
//	/* (non-Javadoc)
//	 * @see org.prelle.rpgtool.sessionscreen.HandoutDisplay#showHandout(java.lang.String)
//	 */
//	@Override
//	public void showHandout(String uri) {
//		logger.warn("TODO: showHandout("+uri+")");
//		
//		ShowHandoutPacket command = new ShowHandoutPacket(uri);
//		client.sendExtensionPacket(command);
//	}
//
//	//----------------------------------------------------------------
//	/* (non-Javadoc)
//	 * @see org.prelle.rpgtool.sessionscreen.HandoutDisplay#getSupportedMimeTypes()
//	 */
//	@Override
//	public List<MimeType> getSupportedMimeTypes() {
//		return mimeTypes;
//	}

}
