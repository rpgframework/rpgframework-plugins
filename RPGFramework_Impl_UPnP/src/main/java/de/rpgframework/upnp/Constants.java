/**
 * 
 */
package de.rpgframework.upnp;

import org.fourthline.cling.model.types.ServiceId;

/**
 * @author prelle
 *
 */
public interface Constants {

	public final static String NAMESPACE = "rpgtool";
	public final static String ID_SESSIONSCREEN = "SessionScreen";
	public final static String ID_GAMEMASTER    = "GameMaster";
	public final static String ID_ContentDirectry   = "ContentDirectory";
	
	public final static ServiceId SERVICE_SESSIONSCREEN = new ServiceId(NAMESPACE, ID_SESSIONSCREEN);
	public final static ServiceId SERVICE_GAMEMASTER    = new ServiceId(NAMESPACE, ID_GAMEMASTER);
	public final static ServiceId SERVICE_MEDIASERVER   = new ServiceId("upnp-org", ID_ContentDirectry);

}
