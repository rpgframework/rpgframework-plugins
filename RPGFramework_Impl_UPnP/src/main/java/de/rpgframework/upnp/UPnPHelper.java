/**
 * 
 */
package de.rpgframework.upnp;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.Executors;

import javax.swing.ImageIcon;

import org.apache.log4j.Logger;
import org.fourthline.cling.UpnpService;
import org.fourthline.cling.model.meta.DeviceDetails;
import org.fourthline.cling.model.meta.RemoteDevice;
import org.fourthline.cling.model.meta.Service;
import org.fourthline.cling.support.model.BrowseFlag;

/**
 * @author prelle
 *
 */
public class UPnPHelper {

	private final static Logger logger = Logger.getLogger("rpgtool.upnp");

	//----------------------------------------------------------------
	public static ImageIcon loadIconFromURI(RemoteDevice device, org.fourthline.cling.model.meta.Icon iconDef) {
		DeviceDetails details = device.getDetails();
		logger.debug("Load icon at "+iconDef+" from device "+details.getFriendlyName());
		logger.debug("device.getPresentationURI = "+details.getPresentationURI());
		
		// if definition comes with data, use it
		if (iconDef.getData()!=null) {
			return new ImageIcon(iconDef.getData());
		}
		
		// No data in description
		logger.info("Icon.uri  = "+iconDef.getUri()+"  isOpaque="+iconDef.getUri().isOpaque());
		// If icon URI is absolut, use it
		if (iconDef.getUri().isAbsolute()) {
			try {
				return new ImageIcon(iconDef.getUri().toURL());
			} catch (MalformedURLException e) {
				logger.error("Failed converting icon URI "+iconDef.getUri()+": "+e);
				return null;
			}
		} else {
			URL iconURL = device.normalizeURI(iconDef.getUri());
			logger.debug("normalized icon URL = "+iconURL);
			return new ImageIcon(iconURL);
		}
		
//		// If BaseURL is present, use it
//		logger.info("device.baseURL = "+details.getBaseURL());
//		if (details.getBaseURL()!=null) {
//			String fullURL = String.format("%s%s", details.getBaseURL().toExternalForm(), iconDef.getUri().toASCIIString());
//			logger.info("Load icon from "+fullURL);
//			try {
//				return new ImageIcon(new URL(fullURL));
//			} catch (MalformedURLException e) {
//				logger.error("Failed converting icon URI "+fullURL+": "+e);
//				return null;
//			}
//		} else if (details.getPresentationURI()!=null) {
//			String fullURL = String.format("%s%s", details.getPresentationURI(), iconDef.getUri().toASCIIString());
//			logger.info("Load icon by presentation URI from "+fullURL);
//			try {
//				return new ImageIcon(new URL(fullURL));
//			} catch (MalformedURLException e) {
//				logger.error("Failed converting icon URI "+fullURL+": "+e);
//				return null;
//			}
//		}
//		
//		
//		return null;
	}
	
//	//----------------------------------------------------------------
//	@SuppressWarnings("rawtypes")
//	public static void backgroundBrowse(Logger logger, UpnpService upnpService, Service service, RemoteFileHandle handle) {
//		logger.warn("Browse container "+handle);
////		FileHandleBrowse browse = new FileHandleBrowse(logger, service, handle, BrowseFlag.DIRECT_CHILDREN);
////
////		Runnable task = new ActionCallbackRunnable(upnpService, browse, 5000);
////		Executors.newSingleThreadExecutor().execute(task);
//	}
//	
//	//----------------------------------------------------------------
//	@SuppressWarnings("rawtypes")
//	public static void backgroundMetaBrowse(Logger logger, UpnpService upnpService, Service service, RemoteFileHandle handle) {
//		logger.warn("BrowseMeta container "+handle);
////		FileHandleBrowse browse = new FileHandleBrowse(logger, service, handle, BrowseFlag.METADATA);
////
////		Runnable task = new ActionCallbackRunnable(upnpService, browse, 5000);
////		Executors.newSingleThreadExecutor().execute(task);
//	}
	
}
