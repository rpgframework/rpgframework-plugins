/**
 * 
 */
package de.rpgframework.upnp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;
import org.fourthline.cling.UpnpService;
import org.fourthline.cling.model.action.ActionInvocation;
import org.fourthline.cling.model.message.UpnpResponse;
import org.fourthline.cling.model.meta.RemoteDevice;
import org.fourthline.cling.model.meta.Service;
import org.fourthline.cling.support.connectionmanager.callback.GetProtocolInfo;
import org.fourthline.cling.support.model.ProtocolInfos;

/**
 * @author Stefan
 *
 */
public class MediaRendererHelper {

	private final static Logger logger = Logger.getLogger("rpgframework.upnp");

	public static List<String> getSupportedProtocols(UpnpService upnpService, Service<RemoteDevice, ?> connectionManager) {
		List<String> supportedProtocols = new ArrayList<String>();

		
		GetProtocolInfo get = new GetProtocolInfo(connectionManager, upnpService.getControlPoint()) {
			
			//--------------------------------------------------------------------
			/**
			 * @see org.fourthline.cling.controlpoint.ActionCallback#failure(org.fourthline.cling.model.action.ActionInvocation, org.fourthline.cling.model.message.UpnpResponse, java.lang.String)
			 */
			@SuppressWarnings("rawtypes")
			@Override
			public void failure(ActionInvocation arg0, UpnpResponse arg1, String arg2) {
				logger.warn("Calling GetProtocolInfo failed");
				// Notify
				synchronized (supportedProtocols) {
					supportedProtocols.notify();
				}
			}
			
			//--------------------------------------------------------------------
			/**
			 * @see org.fourthline.cling.support.connectionmanager.callback.GetProtocolInfo#received(org.fourthline.cling.model.action.ActionInvocation, org.fourthline.cling.support.model.ProtocolInfos, org.fourthline.cling.support.model.ProtocolInfos)
			 */
			@SuppressWarnings("rawtypes")
			@Override
			public void received(ActionInvocation arg0, ProtocolInfos sink,
					ProtocolInfos source) {
				StringTokenizer tok = new StringTokenizer(sink.toString(), ",");
				while (tok.hasMoreTokens()) {
					String support = tok.nextToken();
					supportedProtocols.add(support);
				}
				Collections.sort(supportedProtocols);
				if (logger.isTraceEnabled()) {
					for (String prot : supportedProtocols)
						logger.trace(" Supports "+prot);
				}
				// Notify
				synchronized (supportedProtocols) {
					supportedProtocols.notify();
				}
			}
		};
		
		synchronized (supportedProtocols) {
			upnpService.getControlPoint().execute(get);
			try {
				supportedProtocols.wait(3000);
			} catch (InterruptedException e) {
				logger.warn("Interrupted while waiting for UPnP");
			}
		}

		return supportedProtocols;
	}
}
