/**
 * 
 */
package de.rpgframework.upnp.device;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;

import org.apache.log4j.FileAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.fourthline.cling.UpnpService;
import org.fourthline.cling.model.meta.Icon;
import org.fourthline.cling.model.meta.RemoteDevice;

import de.rpgframework.devices.DeviceFunction;
import de.rpgframework.devices.RPGToolDevice;
import de.rpgframework.upnp.service.UPnPService;

/**
 * @author Stefan
 *
 */
public abstract class UPnPDevice implements RPGToolDevice {

	public Logger logger = Logger.getLogger("rpgframework.upnp");

	protected UpnpService upnpService;
	private List<UPnPService> dynamicServices;
	protected RemoteDevice device;
	private Map<DeviceFunction, Object> functions;
	private byte[] icon;
	
	//--------------------------------------------------------------------
	/**
	 */
	public UPnPDevice(UpnpService upnp, RemoteDevice device) {
		logger = Logger.getLogger("rpgframework.upnp."+device.getDisplayString());
		logger.removeAllAppenders();
		try {
			logger.addAppender(new FileAppender(new PatternLayout("%5p [%c] (%F:%L) - %m%n"), "/tmp/"+device.getDisplayString()+".txt", false));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		functions = new HashMap<DeviceFunction, Object>();
		this.upnpService = upnp;
		this.device = device;
		dynamicServices = new ArrayList<>();
		
		URI presURI = device.getDetails().getPresentationURI();
		URL descURL = device.getIdentity().getDescriptorURL();
		URL baseURL = device.getDetails().getBaseURL();
//		logger.warn("  Pres URI = "+presURI);
//		logger.warn("  Base URL = "+baseURL);
//		logger.warn("  Desc URL = "+descURL);

		String realBaseURL = null;
		if (baseURL!=null) {
			realBaseURL = device.getDetails().getBaseURL().toString();
//		} else if (presURI!=null && presURI.getHost()!=null) {
//			int too = presURI.toString().indexOf("/", 10);
//			if (too<=0)
//				too=presURI.toString().length();
//			realBaseURL = presURI.toString().substring(0, too);
		} else {
			realBaseURL = descURL.toString().substring(0, descURL.toString().indexOf("/", 10));
		}
		logger.trace("  Real Base URL= "+realBaseURL);
		int lastSize = 0;
		for (Icon icon : device.getIcons()) {
			logger.trace("   "+icon.getWidth()+"x"+icon.getHeight()+"  "+icon);
			logger.trace("     may be "+icon.getUri()+"  = "+baseURL+icon.getUri()+"  "+icon.getData());
			if (icon.getWidth()>lastSize) {
				try {
					URL iconURL = new URL(realBaseURL+icon.getUri());
					this.icon = tryLoad(iconURL);
					lastSize = icon.getWidth();
					logger.trace("use icon "+icon.getWidth()+"x"+icon.getHeight()+"  "+iconURL);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}
//		logger.fatal("Stop here");
//		System.exit(0);
	}

	//--------------------------------------------------------------------
	public String toString() {
		return device.getDetails().getFriendlyName();
	}

	//----------------------------------------------------------------
	private byte[] tryLoad(URL location) {
		logger.debug("Load url "+location);
		try {
			HttpURLConnection con = (HttpURLConnection) location.openConnection();
//			con.setRequestProperty("User-Agent", "Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0");
			con.setRequestProperty("Accept-Encoding", "gzip,identity");
//			con.setRequestProperty("DNT", "1");
//			con.setInstanceFollowRedirects(false);
			con.connect();
			
			int code = ((HttpURLConnection)con).getResponseCode();
			logger.trace("  Code was "+code+" for "+location);
			logger.trace("  Content-Type: "+con.getContentType());
			if (code!=200)
				return null;
			if (!con.getContentType().startsWith("image"))
				return null;
			
			// Read all data from stream
			InputStream is = con.getInputStream();
			ByteArrayOutputStream buffer = new ByteArrayOutputStream();
			int nRead;
			byte[] data = new byte[16384];
			while ((nRead = is.read(data, 0, data.length)) != -1) {
			  buffer.write(data, 0, nRead);
			}
			buffer.flush();
			byte[] ret = buffer.toByteArray();
//			logger.debug("  Read "+ret.length+" bytes");
			is.close();
			
			if (con.getContentEncoding()!=null && con.getContentEncoding().equals("gzip")) {
//				logger.warn(" data is GZIPped");
				buffer = new ByteArrayOutputStream();
				ByteArrayInputStream bais = new ByteArrayInputStream(ret); 
				is = new GZIPInputStream(bais);
				while ((nRead = is.read(data, 0, data.length)) != -1) {
					buffer.write(data, 0, nRead);
				}
				buffer.flush();
				ret = buffer.toByteArray();
//				logger.debug("  decompressed to "+ret.length+" bytes");
			}

			
//			try {
//				Thread.sleep(10);
//			} catch (InterruptedException e) {
//			}
//			
//			byte[] ret = null;
//			logger.debug("Try to load "+con.getContentLength()+" bytes");
//			if (con.getContentLength()==-1) {
//				InputStream in = con.getInputStream();
//				if (con.getContentEncoding()!=null && con.getContentEncoding().equals("gzip")) {
//					logger.warn("GZIP");
//					in = new GZIPInputStream(con.getInputStream()); 
//				}
//				
//				byte[] buf = new byte[65536];
//				int off = 0;
//				int read = 0;
//				do {
//					read = in.read(buf, off, 1024);
//					off += read;
//				} while (read==1024);
//				ret = new byte[off];
//				System.arraycopy(buf, 0, ret, 0, off);
//				
//			} else {
//				ret = new byte[con.getContentLength()];
//				InputStream in = con.getInputStream();
//				if (con.getContentEncoding()!=null && con.getContentEncoding().equals("gzip")) {
//					logger.warn("GZIP");
//					in = new GZIPInputStream(con.getInputStream()); 
//				}
//				int pos=0;
//				int read=0;
//				int bytesToRead = con.getContentLength();
//				do {
//					logger.debug("Read "+pos+" - "+bytesToRead+" bytes");
//					read = in.read(ret, pos, bytesToRead);
//					if ( (pos+read) < ret.length )
//						pos+=read;
//					bytesToRead -= read;
//				} while (bytesToRead>0 && read!=-1);
//			}
			logger.debug("Loaded "+ret.length+" bytes from "+location);
//			con.getInputStream().close();
			
			return ret;
		} catch (IOException e) {
			logger.error("Failed loading icon: "+e);
		}
		return null;
	}

	//--------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(RPGToolDevice other) {
		return getName().compareTo(other.getName());
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.devices.RPGToolDevice#getName()
	 */
	@Override
	public String getName() {
		if (device.getDetails()!=null && device.getDetails().getFriendlyName()!=null)
			return device.getDetails().getFriendlyName();
		return device.getDisplayString();
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.devices.RPGToolDevice#getFunction(de.rpgframework.devices.DeviceFunction)
	 */
	@Override
	public Object getFunction(DeviceFunction key) {
		return functions.get(key);
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.devices.RPGToolDevice#getSupportedFunctions()
	 */
	@Override
	public Collection<DeviceFunction> getSupportedFunctions() {
		return functions.keySet();
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.devices.RPGToolDevice#getIcon()
	 */
	@Override
	public byte[] getIcon() {
//		if (icon!=null)
//			logger.debug("getIcon() of "+getName()+" returns "+icon.length+" bytes");
//		else
//			logger.debug("getIcon() of "+getName()+" returns NULL");
		return icon;
	}

	//--------------------------------------------------------------------
	public void addFunction(DeviceFunction key, Object obj) {
		if (obj==null)
			throw new NullPointerException();
		logger.info("Add function "+key+" to "+functions);
		functions.put(key, obj);
	}


	//--------------------------------------------------------------------
	public void addService(UPnPService value) {
		dynamicServices.add(value);
	}

	//--------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	public <T> T getService(Class<T> cls) {
		for (UPnPService serv : dynamicServices) {
			if (cls.isAssignableFrom(serv.getClass()))
				return (T)serv;
		}
		return null;
	}

	//--------------------------------------------------------------------
	public abstract void serviceDetectionFinished();
	

}
