/**
 * 
 */
package de.rpgframework.upnp.device;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.fourthline.cling.UpnpService;
import org.fourthline.cling.model.meta.RemoteDevice;

import de.rpgframework.devices.DeviceFunction;
import de.rpgframework.devices.FunctionSessionHandout;
import de.rpgframework.upnp.FunctionSessionHandoutImpl;
import de.rpgframework.upnp.service.AVTransportService;
import de.rpgframework.upnp.service.ConnectionManagerService;

/**
 * @author prelle
 *
 */
public class UPnPMediaRenderer extends UPnPDevice {
	
	private List<String> supportedProtocols;

	private ConnectionManagerService conMan;
	private AVTransportService avTrans;
	
	//----------------------------------------------------------------
	/**
	 */
	public UPnPMediaRenderer(UpnpService upnpService, RemoteDevice device) {
		super(upnpService, device);
		this.supportedProtocols = new ArrayList<String>();
	}


	//------------------------------------------------------------------
	public List<String> getSupportedProtocols() {
		return supportedProtocols;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.upnp.device.UPnPDevice#serviceDetectionFinished()
	 */
	@Override
	public void serviceDetectionFinished() {
		logger.debug("serviceDetectionFinished");
		conMan = getService(ConnectionManagerService.class);
		if (conMan==null) {
			logger.error("No ConnectionManager in "+getName());
			return;
		}
		avTrans = getService(AVTransportService.class);
		if (avTrans==null) {
			logger.error("No AVTransportService in "+getName());
			return;
		}
		
		supportedProtocols = conMan.getSupportedProtocols();
		
		if (logger.isDebugEnabled()) {
			Collections.sort(supportedProtocols);
			logger.debug("List of supported protocols");
			logger.debug("--------------------------------------------------");
			for (String val : supportedProtocols)
				logger.debug(val);
		}
		
		/* 
		 * Browse supported protocols and find suitable function
		 */
		FunctionSessionHandout sessScreen = null;
		for (String prot : supportedProtocols) {
//			if (prot.startsWith("http-get:*:audio/mpeg:DLNA.ORG_PN=MP3") && sessAudio==null) {
//				
//			}
			if (prot.startsWith("http-get:*:image/jpeg") && (sessScreen==null)) {
				sessScreen = new FunctionSessionHandoutImpl(upnpService, avTrans, conMan);
				addFunction(DeviceFunction.SESSION_HANDOUT, sessScreen);
				logger.info("Device supports showing images");
			}
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.devices.RPGToolDevice#activate()
	 */
	@Override
	public void activate() {
		// TODO Auto-generated method stub
		logger.warn("TODO: activate");
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.devices.RPGToolDevice#deactivate()
	 */
	@Override
	public void deactivate() {
		// TODO Auto-generated method stub
		logger.warn("TODO: deactivate");
	}


}
