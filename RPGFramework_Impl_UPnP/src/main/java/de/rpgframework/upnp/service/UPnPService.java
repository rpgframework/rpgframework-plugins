/**
 * 
 */
package de.rpgframework.upnp.service;

import org.apache.log4j.Logger;
import org.fourthline.cling.UpnpService;
import org.fourthline.cling.model.meta.RemoteDevice;
import org.fourthline.cling.model.meta.RemoteService;
import org.fourthline.cling.model.meta.Service;

/**
 * @author prelle
 *
 */
public abstract class UPnPService {

	protected final static Logger logger = Logger.getLogger("rpgframework.upnp");

	protected RemoteDevice device;
	protected UpnpService upnpService;
	protected RemoteService realService;
	
	//-------------------------------------------------------------------
	public UPnPService(RemoteDevice device, UpnpService upnpService, RemoteService connectionManager) {
		this.device = device;
		this.upnpService = upnpService;
		this.realService = connectionManager;
	}

}
