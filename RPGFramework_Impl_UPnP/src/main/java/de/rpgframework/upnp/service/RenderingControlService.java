package de.rpgframework.upnp.service;

import org.fourthline.cling.UpnpService;
import org.fourthline.cling.model.meta.RemoteDevice;
import org.fourthline.cling.model.meta.RemoteService;

public class RenderingControlService extends UPnPService {

	//-------------------------------------------------------------------
	public RenderingControlService(RemoteDevice device, UpnpService upnpService, RemoteService service) {
		super(device, upnpService, service);
	}

}
