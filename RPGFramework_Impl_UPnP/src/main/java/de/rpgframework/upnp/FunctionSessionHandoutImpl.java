/**
 * 
 */
package de.rpgframework.upnp;

import java.net.URI;
import java.net.URISyntaxException;

import org.apache.log4j.Logger;
import org.fourthline.cling.UpnpService;

import de.rpgframework.devices.FunctionSessionHandout;
import de.rpgframework.media.Media;
import de.rpgframework.upnp.service.AVTransportService;
import de.rpgframework.upnp.service.ConnectionManagerService;

/**
 * @author Stefan
 *
 */
public class FunctionSessionHandoutImpl implements FunctionSessionHandout {

	private final static Logger logger = Logger.getLogger("rpgframework.upnp");

	private UpnpService upnpService;
	private AVTransportService avTransport;
	private ConnectionManagerService connectionManager;
	
	//--------------------------------------------------------------------
	/**
	 */
	public FunctionSessionHandoutImpl(
			UpnpService upnpService, 
			AVTransportService avTransport, 
			ConnectionManagerService connectionManager) {
		this.upnpService = upnpService;
		this.avTransport = avTransport;
		this.connectionManager = connectionManager;
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.devices.FunctionSessionHandout#getSupportedCategories()
	 */
	@Override
	public Media.Category[] getSupportedCategories() {
		// TODO Auto-generated method stub
		return null;
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.devices.FunctionSessionHandout#showHandout(de.rpgframework.media.Media)
	 */
	@Override
	public void showHandout(Media handout) {
		// TODO Auto-generated method stub
		logger.warn("TODO: showHandout "+handout);
		try {
			URI uri = (handout.getLibrary()!=null)?(handout.getLibrary().getLocation()):(handout.getSource().toURI());
			logger.warn("TODO: showHandout "+uri);
			
			if (avTransport.setAVTransportURI(uri.toString())) {
				logger.info("Call play");
				avTransport.play();
			} else {
				logger.warn("Failed setting transport URI");
			}
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
