/**
 * 
 */
package de.rpgframework.upnp;

import org.apache.log4j.Logger;
import org.fourthline.cling.UpnpService;
import org.fourthline.cling.controlpoint.ActionCallback;

/**
 * @author prelle
 *
 */
public class ActionCallbackRunnable implements Runnable {

	private final static Logger logger = Logger.getLogger("rpgtool.upnp");

	ActionCallback callback;
	UpnpService upnpService;
	int timeout;

	//----------------------------------------------------------------
	public ActionCallbackRunnable(UpnpService upnpService, ActionCallback toRun, int timeout) {
		this.callback = toRun;
		this.upnpService = upnpService;
	}

	//----------------------------------------------------------------
	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		synchronized (callback) {
			upnpService.getControlPoint().execute(callback);
			try {
				callback.wait(timeout);
			} catch (InterruptedException e) {
				logger.warn(e.toString());
			}
		}
	}

}
