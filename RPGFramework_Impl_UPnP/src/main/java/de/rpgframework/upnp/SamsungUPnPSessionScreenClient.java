/**
 * 
 */
package de.rpgframework.upnp;

import javax.swing.ImageIcon;

import org.apache.log4j.Logger;
import org.fourthline.cling.UpnpService;
import org.fourthline.cling.model.action.ActionInvocation;
import org.fourthline.cling.model.meta.RemoteDevice;
import org.fourthline.cling.model.meta.RemoteService;
import org.fourthline.cling.model.meta.Service;
import org.fourthline.cling.model.types.InvalidValueException;
import org.fourthline.cling.model.types.ServiceId;

/**
 * @author prelle
 *
 */
public class SamsungUPnPSessionScreenClient extends UPnPFunctionBase implements Constants {

	private final static Logger logger = Logger.getLogger("rpgframework.upnp");

	private RemoteDevice device;
	private RemoteService ssService;
	private ImageIcon icon;

	//------------------------------------------------------------------
	/**
	 */
	SamsungUPnPSessionScreenClient(UpnpService upnp, RemoteDevice device) {
		super(upnp);
		this.device = device;
		
		// Convert icons
		int lastX = 0;
		for (org.fourthline.cling.model.meta.Icon tmp : device.getIcons()) {
			if (tmp.getWidth()>lastX && tmp.getWidth()<100) {
				lastX = tmp.getWidth();
				icon = new ImageIcon(tmp.getData());
			}
		}

		logger.debug("Initialize screen "+device.getDisplayString());

		ssService = device.findService(new ServiceId("samsung.com", "MessageBoxService"));
		if (ssService==null)
			throw new NullPointerException("Device "+device.getDisplayString()+" has no "+ID_SESSIONSCREEN+"-service");
	}

	//------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgtool.SessionScreen#showTextMessage(java.lang.String, int)
	 */
	public void showTextMessage(String message, int seconds) {
		logger.debug("showTextMessage()");

		ActionInvocation<?> invocation =
				new SamsungShowTextMessageActionInvocation(ssService, message, seconds);
		executeAction(invocation);
	}

}

//------------------------------------------------------------------
//------------------------------------------------------------------
@SuppressWarnings("rawtypes")
class SamsungShowTextMessageActionInvocation extends ActionInvocation {

	@SuppressWarnings("unchecked")
	//------------------------------------------------------------------
	SamsungShowTextMessageActionInvocation(Service service, String mess, int seconds) throws InvalidValueException {
		super(service.getAction("AddMessage"));
		setInput("MessageID", "1");
		setInput("MessageType", "text/xml; charset=\"utf-8\"");
		
//		StringBuffer buf = new StringBuffer("&lt;Category&gt;SMS&lt;/Category&gt;"+
//          "&lt;DisplayType&gt;Maximum&lt;/DisplayType&gt;"+
//          "&lt;ReceiveTime&gt;"+
//          "&lt;Date&gt;2010-05-04&lt;/Date&gt;"+
//          "&lt;Time&gt;01:02:03&lt;/Time&gt;"+
//          "&lt;/ReceiveTime&gt;"+
//          "&lt;Receiver&gt;"+
//          "&lt;Number&gt;12345678&lt;/Number&gt;"+
//          "&lt;Name&gt;Receiver&lt;/Name&gt;"+
//          "&lt;/Receiver&gt;"+
//          "&lt;Sender&gt;"+
//          "&lt;Number&gt;11111&lt;/Number&gt;"+
//          "&lt;Name&gt;Sender&lt;/Name&gt;"+
//          "&lt;/Sender&gt;"+
//          "&lt;Body&gt;Hello World!!!&lt;/Body&gt;"); 
		StringBuffer buf = new StringBuffer("<Category>SMS</Category>"+
		          "<DisplayType>Maximum</DisplayType>"+
		          "<ReceiveTime>"+
		          "<Date>2010-05-04</Date>"+
		          "<Time>01:02:03</Time>"+
		          "</ReceiveTime>"+
		          "<Receiver>"+
		          "<Number>12345678</Number>"+
		          "<Name>Receiver</Name>"+
		          "</Receiver>"+
		          "<Sender>"+
		          "<Number>11111</Number>"+
		          "<Name>Sender</Name>"+
		          "</Sender>"+
		          "<Body>Hello World!!!</Body>"); 
		setInput("Message", buf.toString());
	}
}
