/**
 * 
 */
package de.rpgframework.upnp;

import org.apache.log4j.Logger;
import org.fourthline.cling.UpnpService;
import org.fourthline.cling.controlpoint.ActionCallback;
import org.fourthline.cling.model.action.ActionInvocation;
import org.fourthline.cling.model.message.UpnpResponse;

/**
 * @author Stefan
 *
 */
public abstract class UPnPFunctionBase {

	private final static Logger logger = Logger.getLogger("rpgframework.upnp");
	
	protected UpnpService upnpService;

	//--------------------------------------------------------------------
	/**
	 */
	public UPnPFunctionBase(UpnpService service) {
		this.upnpService = service;
	}
	//------------------------------------------------------------------
	protected void executeAction(ActionInvocation<?> invocation) {
		// Executes asynchronous in the background
		upnpService.getControlPoint().execute(
				new ActionCallback(invocation) {

					@SuppressWarnings("rawtypes")
					@Override
					public void success(ActionInvocation invocation) {
						assert invocation.getOutput().length == 0;
						logger.debug("Successfully called action!");
					}

					@SuppressWarnings("rawtypes")
					@Override
					public void failure(ActionInvocation invocation,
							UpnpResponse operation,
							String defaultMsg) {
						logger.warn(defaultMsg);
					}
				}
				);

	}

}
